import matplotlib.pyplot as plt
import matplotlib
import math
from matplotlib.gridspec import GridSpec
import csv
INPUT = "avgGen_mutHigh_food_2_64.csv"
INPUT2 = "avgGen_mutHigh_food_32_4.csv"
INPUT3 = "avgGen_mutLow_food_2_64.csv"
INPUT4 = "avgGen_mutLow_food_32_4.csv"

INPUTP = "avgGen_mutHigh_pred_45_140.csv"
INPUTP2 = "avgGen_mutHigh_pred_135_70.csv"
INPUTP3 = "avgGen_mutLow_pred_45_140.csv"
INPUTP4 = "avgGen_mutLow_pred_135_70.csv"

INPUTL = "avgDeltaPref.csv"
INPUTL1 = "conf_1.csv"
INPUTL2 = "conf_5.csv"
INPUTL3 = "conf_10.csv"
INPUTL4 = "conf_15.csv"
INPUTL5 = "conf_-10_3_1.csv"
INPUTL6 = "conf_-10_4_3.csv"
INPUTL7 = "conf_-10_5_5.csv"
INPUTL8 = "conf_11_3_1.csv"
INPUTL9 = "conf_11_4_3.csv"
INPUTL10 = "conf_11_5_5.csv"
INPUTL11 = "conf_-11_3_1.csv"
INPUTL12 = "conf_-11_4_3.csv"
INPUTL13 = "conf_-11_5_5.csv"


def parseanddrawfood():
    global INPUT
    matplotlib.rcParams.update({'font.size': 5})
    s = "high mut small clust"
    i = 0
    maxis = 8
    plt.figure(1)
    height = math.ceil(maxis/2)
    width = 2
    gs = GridSpec(height, width)
    axeslist = []
    for j in range (maxis):
        axeslist.append(plt.subplot(gs[j]))
    for a1,a2 in zip(axeslist[0::2],axeslist[1::2]):
        speed = []
        foodpref = []
        noise = []
        antipred = []
        timeneeded = []
        zoa = []
        zoo = []
        blind = []
        turn = []
        with open(INPUT,'r') as csvfile:
            dreader = csv.DictReader(csvfile)
            for row in dreader:
                speed.append(float(row["Speed"]))
                foodpref.append(float(row["food pref"]))
                noise.append(float(row["noise"]))
                antipred.append(float(row["antipred"]))
                zoa.append(float(row["zoa"]))
                zoo.append(float(row["zoo"]))
                blind.append(float(row["blind"]))
                turn.append(float(row["turn"]))
                timeneeded.append(float(row["timeNeeded"]))
        a1.set_ylabel(s)
        a2.set_ylabel(s)
        a1.plot(speed,label = "Speed")
        a1.plot(foodpref,label = "Foodpref")
        a1.plot(noise,label = "Noise")
        a1.plot(antipred,label = "antipred")
        a2.plot(timeneeded,label = "time")
        a2.plot(zoa,label = "zoa")
        a2.plot(zoo,label = "zoo")
        a2.plot(blind,label = "blind")
        a2.plot(turn,label= "turn")
        a1.legend(loc = "upper left")
        a2.legend(loc = "upper left")
        if i == 0:
            INPUT = INPUT2
            s = "high mut big clust"
            s = "high mut big clust"
        elif i == 1:
            INPUT = INPUT3
            s = "low mut small clust"
            s = "low mut small clust"
        elif i == 2:
            INPUT = INPUT4
            s= "low mut big clust"
            s = "low mut big clust"
        elif i == 3:
            True
        elif i ==4:
            True
        i += 1
    plt.show()


def parseanddrawpred():
    global INPUTP
    matplotlib.rcParams.update({'font.size': 5})
    s = "high mut good Pred"
    i = 0
    maxis = 8
    plt.figure(2)
    height = math.ceil(maxis/2)
    width = 2
    gs = GridSpec(height, width)
    axeslist = []
    for j in range (maxis):
        axeslist.append(plt.subplot(gs[j]))
    for a1,a2 in zip(axeslist[0::2],axeslist[1::2]):
        speed = []
        foodpref = []
        noise = []
        antipred = []
        timeneeded = []
        zoa = []
        zoo = []
        blind = []
        turn = []
        with open(INPUTP,'r') as csvfile:
            dreader = csv.DictReader(csvfile)
            for row in dreader:
                speed.append(float(row["Speed"]))
                foodpref.append(float(row["food pref"]))
                noise.append(float(row["noise"]))
                antipred.append(float(row["antipred"]))
                zoa.append(float(row["zoa"]))
                zoo.append(float(row["zoo"]))
                blind.append(float(row["blind"]))
                turn.append(float(row["turn"]))
                timeneeded.append(float(row["timeNeeded"]))
        a1.set_ylabel(s)
        a2.set_ylabel(s)
        a1.plot(speed,label = "Speed")
        a1.plot(foodpref,label = "Foodpref")
        a1.plot(noise,label = "Noise")
        a1.plot(antipred,label = "antipred")
        #a2.plot(timeneeded,label = "time")
        a2.plot(zoa,label = "zoa")
        a2.plot(zoo,label = "zoo")
        a2.plot(blind,label = "blind")
        a2.plot(turn,label= "turn")
        a1.legend(loc = "upper left")
        a2.legend(loc = "upper left")
        if i == 0:
            INPUTP= INPUTP2
            s = "high mut bad Pred"
        elif i == 1:
            INPUTP = INPUTP3
            s = "low mut good Pred"
        elif i == 2:
            INPUTP = INPUTP4
            s = "low mut bad Pred"
        elif i == 3:
            True
        elif i ==4:
            True
        i += 1

    plt.show()


def parseanddrawleader():
    global INPUTL
    matplotlib.rcParams.update({'font.size': 5})
    s = "high mut good Pred"
    maxis = 8
    plt.figure(2)
    height = math.ceil(maxis/2)
    width = 2
    gs = GridSpec(height, width)
    axeslist = []
    i= 1
    INPUTL = INPUTL1
    j= 0 
    for j in range (maxis):
        axeslist.append(plt.subplot(gs[j]))
    for a in axeslist:
        avgdeltaangle = []
        confusion1 = []
        confusion2 = []
        confusion3 = []
        confusion4 = []
        with open(INPUTL,'r') as csvfile:
            dreader = csv.DictReader(csvfile)
            for row in dreader:
                if i == 0:
                    avgdeltaangle.append(float(row[0]))
                if i >= 1 and i<= 4:
                    confusion1.append(float(row['Confusion']))
        if i == 1:            
            INPUTL = INPUTL2
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion2.append(float(row['Confusion']))
                INPUTL = INPUTL3
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion3.append(float(row['Confusion']))
                INPUTL = INPUTL4
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion4.append(float(row['Confusion']))
        if i == 2:
            INPUTL = INPUTL6
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion2.append(float(row['Confusion']))
                INPUTL = INPUTL7
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion3.append(float(row['Confusion']))
        if i == 3:
            INPUTL = INPUTL9
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion2.append(float(row['Confusion']))
                INPUTL = INPUTL10
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion3.append(float(row['Confusion']))
        if i == 4:
            INPUTL = INPUTL12
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion2.append(float(row['Confusion']))
                INPUTL = INPUTL13
            with open(INPUTL,'r') as csvfile:
                dreader = csv.DictReader(csvfile)
                for row in dreader:
                    confusion3.append(float(row['Confusion']))
        
        if i == 0:
            a.plot(avgdeltaangle)
            INPUTL = INPUTL1

        elif i == 1:
            a.plot(confusion1,label = "confusion1")
            a.plot(confusion2,label = "confusion5")
            a.plot(confusion3,label = "confusion10")
            a.plot(confusion4,label = "confusion15")
            confusion1= []
            confusion2=[]
            confusion3=[]
            confusion4=[]
            a.legend(loc = "upper left")
            INPUTL = INPUTL5
        elif i == 2:
            a.plot(confusion1)
            a.plot(confusion2)
            a.plot(confusion3)
            a.plot(confusion4)
            confusion1= []
            confusion2=[]
            confusion3=[]
            confusion4=[]
            a.legend(loc = "upper left")
            INPUTL = INPUTL8
        elif i == 3:
            a.plot(confusion1)
            a.plot(confusion2)
            a.plot(confusion3)
            a.plot(confusion4)
            confusion1= []
            confusion2=[]
            confusion3=[]
            confusion4=[]
            a.legend(loc = "upper left")
            INPUTL = INPUTL11
        elif i == 4:
            a.plot(confusion1)
            a.plot(confusion2)
            a.plot(confusion3)
            a.plot(confusion4)
            confusion1= []
            confusion2=[]
            confusion3=[]
            confusion4=[]
            a.legend(loc = "upper left")
        a.set_ylabel(s)
        i += 1
    plt.show()

parseanddrawleader()
parseanddrawfood()
parseanddrawpred()
