import time
import random
from graphics import * # get here: http://mcsp.wartburg.edu/zelle/python/graphics.py

import matplotlib.pyplot as plt
import numpy as np

# imports from our files
from help_functions import *
from classes import Agent
from check_boundarys import checkBoundary

# ------------------------------------------------------------
# global variables

# whether average distance will be ploted at end of simulation
plot =  True
# window dimensions
winWidth = 700
winHeight = 500

window = GraphWin("Window", winWidth, winHeight)

# max time for one simulation
maxTime = 4000

# max velocity of the agents
maxV = 8
# number of agents
agentNum = 75

# -------------------------------------------------------------
# simplest simulation


# updateVelociy for simplest simulation
def updateV(agent, nearestNeighbour):

    # actual math
    vx = agent.x_velocity + 0.1 * nearestNeighbour.x_velocity + random.uniform(-3,3)
    vy = agent.y_velocity + 0.1 * nearestNeighbour.y_velocity + random.uniform(-3,3)

    # check new velocity against max velocity
    if(abs(vx) < maxV) :
        agent.set_xvelocity(vx)
    elif (vx <= -maxV):
        agent.set_xvelocity(-maxV)
    else :
        agent.set_xvelocity(maxV)

    if(abs(vy) < maxV ):
        agent.set_yvelocity(vy)
    elif (vy <= -maxV):
        agent.set_yvelocity(-maxV)
    else :
        agent.set_yvelocity(maxV)
        
    return agent

# update all agents with simplest simulation
def update_simplest(agents):
    for agent in agents:
        nearestNeighbour = nearest_neighbour(agent, agents)
        agent = updateV(agent, nearestNeighbour)

        agent = checkBoundary(agent, winWidth, winHeight)
        agent.draw_Line()
    return agents

# -------------------------------------------------------------
# main

def main():

    agents = []

    if plot:
        # data for plotting
        distancedata = np.array([])
        distancestd = np.array([])
        numpycount = np.array([])


    # generate agents
    for i in range(agentNum):

        agent = Agent(Point(random.uniform(0,winWidth), random.uniform(0,winHeight)), window, maxV)
        # give them a random starting direction
        agent.set_xvelocity(random.uniform(-2,2))
        agent.set_yvelocity(random.uniform(-2,2))
        agent.draw_Line()
        
        agents.append(agent)

    # main loop
    for i in range(maxTime):
        if plot:
            rawdata = totalavgdistance(agents,distancedata)
            #print ("rawdata: "+ str(rawdata))
            distancedata = np.append(distancedata,np.mean(rawdata))
            distancestd = np.append(distancestd,np.std(rawdata))
            numpycount = np.append(numpycount,i)
 
        
        agents = update_simplest(agents)
        time.sleep(0.01)


    # show plot of average distance
    if plot:
        plt.errorbar(numpycount,distancedata,yerr=distancestd)
        plt.show()
 
    window.getMouse()
    window.close()

main()
