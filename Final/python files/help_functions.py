import math
import numpy as np

# -------------------------------------------------------------------
# help functions

# Distance function betwen points xi, yi and xii,yii
def distance(xi,xii,yi,yii):
    sq1 = (xi-xii)*(xi-xii)
    sq2 = (yi-yii)*(yi-yii)
    return math.sqrt(sq1 + sq2)

# absolute value of a two dimensional vector
def absvec(a, b):
    absV = math.sqrt(a*a + b*b)
    if absV == 0: absV = 0.001
    return absV

# angle between two vector´s in degrees
def calc_angle(x1, y1, x2, y2):
        skalar = x1*x2 + y1*y2
        abs1 = absvec(x1, y1)
        abs2 = absvec(x2, y2)

        erg = skalar/(abs1 * abs2)
        if erg > 1:
            erg = 1

        elif erg < -1:
            erg = -1
        return math.degrees(math.acos(erg))


# find nearest neighbour of a given Agent
def nearest_neighbour(agent, agents):
    minDis = float('inf')
    nearestNeighbour = None

    for neighbour in agents:
        if (agent == neighbour):
            True
        elif (nearestNeighbour == None):
            nearestNeighbour = neighbour
        else:
            dis = distance(agent.point.getX(),neighbour.point.getX(), agent.point.getY(), neighbour.point.getY())
            if(dis < minDis):
                minDis = dis
                nearestNeighbour = neighbour
                
    return nearestNeighbour


# --------------------------------------------------
# used to plot

def avgdistancetoall(agent,agents):
    i= 0
    totaldistance = 0
    for i in range (len (agents)):
        if agents[i] == agent:
            True
        else:
            totaldistance += distance (agent.point.getX(),agent.point.getY(),agents[i].point.getX(),agents[i].point.getY())
    avgdistance = totaldistance/i
    return avgdistance


def totalavgdistance(agents, numplist):
    for agent in agents:
        numplist = np.append(numplist, avgdistancetoall(agent,agents))
    return numplist

