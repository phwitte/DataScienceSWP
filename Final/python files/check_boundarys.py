from graphics import * # get here: http://mcsp.wartburg.edu/zelle/python/graphics.py
import math


# check for window boundaries and move agent
# window walls "reflect" agents
def checkBoundary(agent, winWidth, winHeight):
    point = agent.point
    point.move(agent.x_velocity, agent.y_velocity)

    x = point.getX()
    y = point.getY()

    # agent is in window
    if x > 0 and y < winHeight and x < winWidth and y > 0:
        agent.set_point(point)
        
    # window walls "reflect" agents
    elif x <= 0 or x >= winWidth:
        agent.set_xvelocity(agent.x_velocity * (-1))
        agent.point.move(agent.x_velocity, agent.y_velocity)

    elif y <= 0 or y >= winHeight:
        agent.set_yvelocity(agent.y_velocity * (-1))
        agent.point.move(agent.x_velocity, agent.y_velocity)
        
    return agent

# check for window boundaries and move agent
# agents swim through window walls(wraparound)
def checkBoundary_free(agent, winWidth, winHeight):
    point = agent.point
    point.move(agent.x_velocity, agent.y_velocity)

    x = point.getX()
    y = point.getY()

    # agent is in window
    if x > 0 and y < winHeight and x < winWidth and y > 0:
        agent.set_point(point)

    # wraparound
    elif x <= 0 or x >= winWidth:
        agent.point.undraw()
        x = agent.point.getX() % winWidth
        agent.set_point(Point(x, y))

    elif y <= 0 or y >= winHeight:
        agent.point.undraw()
        y = agent.point.getY() % winHeight
        agent.set_point(Point(x, y))
        
    return agent

# check if rotation is in range of maxTurn, set to +/-maxturn if not,
# else make velocity = tempvelocity
def move_agent_couzin(agent, maxTurn, radTurn, negRadTurn):
    angle_old = math.atan2(agent.y_velocity, agent.x_velocity)
    angle_new = math.atan2(agent.tempvelocity[1], agent.tempvelocity[0])
    alpha = math.degrees(angle_old - angle_new)
    
    # test if in range of maxturn, if not rotate angle by maxTurn in
    # direction of new direction
    if abs(alpha) > 180:
        if alpha < 0:
            alpha += 360
        else:
            alpha -= 360

    if abs(alpha) < maxTurn:
        x = agent.tempvelocity[0]
        y = agent.tempvelocity[1]
    elif alpha < 0:
        x =  agent.x_velocity * math.cos(radTurn) - agent.y_velocity  * math.sin(radTurn)
        y =  agent.x_velocity * math.sin(radTurn) + agent.y_velocity  * math.cos(radTurn)
    else:
        x =  agent.x_velocity * math.cos(negRadTurn) - agent.y_velocity  * math.sin(negRadTurn)
        y =  agent.x_velocity * math.sin(negRadTurn) + agent.y_velocity  * math.cos(negRadTurn)

    agent.x_velocity = x
    agent.y_velocity = y
    
    return agent
