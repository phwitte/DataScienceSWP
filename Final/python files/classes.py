from graphics import * # get here: http://mcsp.wartburg.edu/zelle/python/graphics.py
import random
import math


# -------------------------------------------------------------
# classes

# Agent class represents normal Agents/Fishes
# used in:  simplest simulation
#           viczek
#           couzin
#           leadership

# point: location of Agent
# line/colour: for displaying purposes
# tempvelocity: x/y velocity for next step, temporary
# prefdir: prefered direction of the Agent
# weight: weight to quantify prefdir and actuall direction
# maxV: max velocity
class Agent:
    def __init__(self,point,window,maxV):
        self.point = point
        self.colour = "black"
        self.window = window
        self.x_velocity = 0
        self.y_velocity = 0
        self.line = Line(point,Point(point.getX() + self.x_velocity,point.getY() +self.y_velocity))
        self.tempvelocity = [0,0]
        self.prefdir = [0,0]
        self.weight = 0
        self.speed = 3
        self.maxV = maxV

    # setter
    def set_temp_velocity(self,temp_velocity):
        self.tempvelocity = temp_velocity

    def set_colour(self,colour):
        self.colour = colour

    def set_xvelocity(self,xvelocity):
        self.x_velocity = xvelocity
        
    def set_yvelocity(self,yvelocity):
        self.y_velocity=yvelocity

    def set_prefdir(self,prefdir):
        self.prefdir = prefdir

    def set_weight(self,weight):
        self.weight = weight

    def set_point(self,point):
        self.point = point

    # function to draw agent to window 
    def draw_Line(self):
        self.line.undraw()
        self.line = Line(self.point,Point(self.point.getX() + self.x_velocity,self.point.getY() +self.y_velocity))
        self.line.setArrow("last")
        self.line.setFill(self.colour)
        self.line.draw(self.window)

        
# -------------------------------------------------------------

# AcklandAgent class represents normal Agents/Fishes
# used in: Ackland simulation
class AcklandAgent:
    def __init__(self, point, window):

        # point: location of Agent
        # tempV: x/y velocity for next step, temporary
        # line/colour: for displaying purposes
        
        self.color = color_rgb(0,0,0)
        self.point = point
        self.window = window
        self.x_velocity = 1
        self.y_velocity = 1
        self.tempvelocity = [0,0]
        self.line = Line(self.point, Point(self.point.getX() + self.x_velocity, self.point.getY()+self.y_velocity))

        self.zor_r = 5
            
        #evolvable
        # speed: fixed speed of that agent
        # zo(r/o/a)_r: radius of that zone for this agent
        self.speed = 4
        self.zoo_r = 20
        self.zoa_r = 200
        
        # self.attCircle = Circle(self.point, self.zoa_r)

        # blind angle: angle behint the agent where he can´t see
        # turn angle: max angle with wich the agent can turn in one step
        # both in degrees, and are actually double that size
        # (from 0/180 degrees in both directions)
        self.blind_angle = 30
        self.turn_angle = 50

        # food pref: how strongly the agent wants to go to the nearest food
        # anti pred: how strongly the agent avoids predators
        self.food_pref = 5
        self.anti_pred = 0

        # foodLevel: how many food paticle this agent has eaten
        self.foodLevel = 0
        # random noise on top of movement
        self.noise = 2

    # setter
    def set_xvelocity(self,xvelocity):
        self.x_velocity = xvelocity
        
    def set_yvelocity(self, yvelocity):
        self.y_velocity = yvelocity

    def set_point(self,point):
        self.point = point    

    def drawLine(self):
        self.line.undraw()
        self.line = Line(self.point, Point(self.point.getX() + self.x_velocity, self.point.getY()+self.y_velocity))
        self.line.setArrow("last")
        self.line.setFill(self.color)
        self.line.draw(self.window)

    def create_random_agent(self, zor_r, As, Am):
        self.y_velocitx = random.uniform(-1, 1)
        self.y_velocity = random.uniform(-1, 1)

        self.zor_r = zor_r
        self.zoa_r = random.uniform( math.sqrt(As/(2*math.pi)), 1.5*math.sqrt(As/(2*math.pi)) )
        self.zoo_r = random.uniform(zor_r, self.zoa_r)

        self.speed = random.uniform(1, 5)
        self.blind_angle = (360 - math.degrees(As/(self.zoa_r**2)) ) / 2
        self.turn_angle = math.degrees(Am/(2*(self.speed**2)))

        self.food_pref = random.uniform(0,5)
        self.anti_pred = random.uniform(0,5)

        self.noise = random.uniform(0,1)

        r = random.randrange(256)
        g = random.randrange(256)
        b = random.randrange(256)

        return self

"""     # draw circle(zoa) around agent
        self.attCircle.undraw()
        self.attCircle = Circle(self.point, self.zoa_r)
        self.attCircle.setOutline("black")
        self.attCircle.draw(self.window)
"""
# ------------------------------------------------------------------
# Predator class
# used in: Ackland simulation

class Predator:

    def __init__(self, point, window ):
        # point: location of Agent
        # tempV: x/y velocity for next step, temporary
        # line/colour: for displaying purposes
        self.color = "red"
        self.point = point
        self.window = window
        self.x_velocity = 1
        self.y_velocity = 1
        self.tempvelocity = [0,0]

        self.line = Line(self.point, Point(self.point.getX() + self.x_velocity, self.point.getY()+self.y_velocity))

        self.zor_r = 10     #fix
        
        #evolvable
        self.zoa_r = 200
        self.speed = 5.5

       # self.attCircle = Circle(self.point, self.zoa_r)

        # blind angle: angle behint the agent where he can´t see
        # turn angle: max angle with wich the agent can turn in one step
        # both in degrees, and are actually double that size
        # (from 0/180 degrees in both directions)
        self.blind_angle = 135
        self.turn_angle = 50

        self.noise = 2
        # if predator has eaten an agent
        self.hasEaten = False
        # how old predator is(needed for killing him if he does not eat)
        self.lifeTime = 0

    # setter
    def set_xvelocity(self,xvelocity):
        self.x_velocity = xvelocity
        
    def set_yvelocity(self,yvelocity):
        self.y_velocity = yvelocity

    def set_point(self,point):
        self.point = point         

    def drawLine(self):
        self.line.undraw()
        self.line = Line(self.point, Point(self.point.getX() + self.x_velocity, self.point.getY()+self.y_velocity))
        self.line.setArrow("last")
        self.line.setFill(self.color)
        self.line.draw(self.window)

    def create_random_predator(self, zor_r, As):
        self.x_velocity = random.uniform(-1, 1)
        self.y_velocity = random.uniform(-1, 1)

        self.zor_r = zor_r
        view_angle = 360 - (self.blind_angle*2)
        self.zoa_r = 20 * math.sqrt(As/view_angle)
        self.turn_angle = 140

        return(self)

# ------------------------------------------------------------------
# Food class
# used in: Ackland simulation

class Food:
    def __init__(self, Point):
        self.point = Point

