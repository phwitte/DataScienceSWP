This is the result of the "Data Science" Softwarproject 2017 by group "one".

to run, the simulation require this file:	 http://mcsp.wartburg.edu/zelle/python/graphics.py

there are 4 simulations, build after the contents of different papers:
	simplest		just a very simple simulation to get startet
				(two versions of this, simplest_vergleich is working fine, simplest_simulation
				should do exactly the same, but doesn�t, and i don�t know why....)
	couzin/vicsek		paper:	Collective memory and spatial sorting in animal groups. (Couzin)
					Novel Type of Phase Transition in a System of Self-Driven Particles (Vicsek)
	leadership		paper: 	Effective leadership and decision- making in animal groups on the move 
					(Couzin)
	ackland			paper: 	Evolving the selfish herd: emergence of distinct aggregating strategies in an 
					individual-based model (Wood, A. J; Ackland, G. J)

to run these simulations, simply put all the python-files present in the "python files"-folder into a folder on your pc, add the file linked above, compile with python version 3.6, and run.

there are various global variables in each simulation, which can be used to change the simulation, each briefly explained in the simulation-files themselfs. for more information on some of them, it is advised to read the corresponding paper ;)

the final presentation and some datasets can also be found in this folder, as well as some videas of the simulation.