from graphics import *
import time
import random
import math
import matplotlib.pyplot as plt
import numpy as np
import csv
TIMESTEPS = "leader_1_4_50_02.csv"
# ------------------2------------------------------------------
# classes
class Agent:
    def __init__(self,point,window,maxV):
        self.point = point
        self.colour = "black"
        self.window = window
        self.x_velocity = 0
        self.y_velocity = 0
        self.line = Line(point,Point(point.getX() + self.x_velocity,point.getY() +self.y_velocity))
        self.tempvelocity = [0,0]
        self.prefdir = [0,0]
        self.weight = 0
        self.speed = 3
        self.maxV = maxV


    def set_temp_velocity(self,temp_velocity):
        self.tempvelocity = temp_velocity

    def set_colour(self,colour):
        self.colour = colour

    def set_xvelocity(self,xvelocity):
        self.x_velocity = xvelocity
        
    def set_yvelocity(self,yvelocity):
        self.y_velocity=yvelocity

    def set_prefdir(self,prefdir):
        self.prefdir = prefdir

    def set_weight(self,weight):
        self.weight = weight

    def set_point(self,point):
        self.point = point
        
    def draw_Line(self):
        self.line.undraw()
        self.line = Line(self.point,Point(self.point.getX() + self.x_velocity,self.point.getY() +self.y_velocity))
        self.line.setArrow("last")
        self.line.setFill(self.colour)
        self.line.draw(self.window)
        
        




# -------------------------------------------------------------------
# help functions

# Distance function betwen points xi, yi and xii,yii
def distance(xi,xii,yi,yii):
    sq1 = (xi-xii)*(xi-xii)
    sq2 = (yi-yii)*(yi-yii)
    return math.sqrt(sq1 + sq2)

# -------------------------------------------------------------------
# simplest simulation

def nearest_neighbour(a,aas):

    minDis = float('inf')
    nn = None

    for b in aas:
        if (a == b):
            True
        elif (nn == None):
            nn = b
            minDis = distance(a.point.getX(),b.point.getX(), a.point.getY(), b.point.getY())
        else:
            dis = distance(a.point.getX(),b.point.getX(), a.point.getY(), b.point.getY())
            if(dis < minDis):
                minDis = dis
                nn = b
    return nn

# updateVelociy
def updateV(agent, nn):
    maxV = agent.maxV
    vx = agent.x_velocity+ 0.1*nn.x_velocity + random.uniform(-3, 3)
    vy = agent.y_velocity + 0.1*nn.y_velocity + random.uniform(-3, 3)

    if(abs(vx) < maxV) :
        agent.set_xvelocity(vx)
    elif (vx <= -maxV):
        agent.set_xvelocity(-maxV)
    else :
        agent.set_xvelocity(maxV)

    if(abs(vy) < maxV ):
        agent.set_yvelocity(vy)
    elif (vy <= -maxV):
        agent.set_yvelocity(-maxV)
    else :
        agent.set_yvelocity(maxV)
    return agent

# check for window boundaries
def checkBoundary(agent, winWidth, winHeight):
    point = agent.point
    point.move(agent.x_velocity,agent.y_velocity)

    x = point.getX()
    y = point.getY()

    if x > 0 and y < winHeight and x < winWidth and y > 0:
        agent.set_point(point)

    elif x <= 0 or x >= winWidth:
        agent.set_xvelocity(agent.x_velocity * (-1))
        agent.point.move(agent.x_velocity,agent.y_velocity)

    elif y <= 0 or y >= winHeight:
        agent.set_yvelocity(agent.y_velocity * (-1))
        agent.point.move(agent.x_velocity,agent.y_velocity)
    return agent


# check for window boundaries and move agents
def checkBoundary_free(agent, winWidth, winHeight):
    point = agent.point
    point.move(agent.x_velocity,agent.y_velocity)

    x = point.getX()
    y = point.getY()

    if x > 0 and y < winHeight and x < winWidth and y > 0:
        agent.set_point(point)

    elif x <= 0 or x >= winWidth:
        #agent[1] = agent[1] % winWidth
        agent.point.undraw()
        x = agent.point.getX() % winWidth
        agent.set_point(Point(x, y))

        #agent[0].move(agent[1],agent[2])

    elif y <= 0 or y >= winHeight:
        #agent[2] = agent[2] % winHeight
        #agent[0].move(agent[1],agent[2])
        agent.point.undraw()
        y = agent.point.getY() % winHeight
        agent.set_point(Point(x, y))
    return agent

def update_simplest(agents, maxV, winWidth, winHeight, window):
    for agent in agents:
        nn = nearest_neighbour(agent, agents)
        agent = updateV(agent, nn)
        
        agent = checkBoundary(agent, winWidth, winHeight)
        agent.draw_Line()
        """agent[3].undraw()
        agent[3] = Line(agent[0], Point(agent[0].getX() + agent[1], agent[0].getY() + agent[2]))
        agent[3].setArrow("last")
        agent[3].draw(window)
        """
    return agents
# -------------------------------------------------------------------
#  couzin and vicsek simulation

def absvec(a, b):
    m = math.sqrt(a*a + b*b)
    if m == 0: m = 0.001
    return m

def calc_angle(x1, y1, x2, y2):
        skalar = x1*x2 + y1*y2
        abs1 = absvec(x1, y1)
        abs2 = absvec(x2, y2)

        erg = skalar/(abs1* abs2)
        if erg > 1:
            #print erg
            erg=1

        elif erg < -1:
            #print erg
            erg=-1
        return math.degrees(math.acos(erg))

# returns three lists, one for each zone,
# contaning all other agent in the zone.
# ignores al egents ind the angle behind the current agent defined by blind.
def neigbour_in_zones(a, aas, zor_r, zoo_r, zoa_r, blind):
    zor = []
    zoo = []
    zoa = []


    for agent in aas:
        disVecX = agent.point.getX() - a.point.getX()
        disVecY = agent.point.getY() - a.point.getY()
        alpha = calc_angle(a.x_velocity,a.y_velocity, disVecX, disVecY)

        if (a == agent):
            True
        elif alpha < 180 - blind and alpha > 180 + blind:
            True
        else:
            dis = absvec(agent.point.getX() - a.point.getX() , agent.point.getY() - a.point.getY() )
            if dis <= zor_r:
                zor.append(agent)
            elif dis <= zoo_r:
                zoo.append(agent)
            elif dis <= zoa_r:
                zoa.append(agent)

    #print len(zoo)+len(zor)+len(zoa)
    return [zor, zoo, zoa]

def wraparound_distance(x1, y1, x2, y2, winWidth, winHeight):
    xDis = x1-x2
    yDis = y1-y2

    x=x1
    y=y1
    flag = False

    if(xDis > winWidth/2):
        x -= winWidth
        flag = True
    elif (xDis < -winWidth/2):
        x += winWidth
        flag = True
    if(yDis > winHeight/2):
        y -= winHeight
        flag = True
    elif (yDis < -winHeight/2):
        y += winHeight
        flag = True

    return [distance(x, y, x2, y2), flag, x-x2, y-y2]

def neigbour_in_zones_free(a, aas, zor_r, zoo_r, zoa_r, blind, winWidth, winHeight):
    zor = []
    zoo = []
    zoa = []

    for agent in aas:
        wpd = wraparound_distance(a.point.getX(), a.point.getY(), agent.point.getX(), agent.point.getY(), winWidth, winHeight)
        dis = wpd[0]
        disVecX = wpd[2]
        disVecY = wpd[3]
        alpha = calc_angle(a.x_velocity,a.y_velocity, disVecX, disVecY)

        if (a == agent):
            True
        elif alpha < 180 - blind and alpha > 180 + blind:
            True
        else:
            dis = absvec(agent.point.getX() - a.point.getX() , agent.point.getY() - a.point.getY() )
            if dis <= zor_r:
                zor.append([agent, wpd[1]])
            elif dis <= zoo_r:
                zoo.append([agent, wpd[1]])
            elif dis <= zoa_r:
                zoa.append([agent, wpd[1]])

    #print len(zoo)+len(zor)+len(zoa)
    return [zor, zoo, zoa]

#update Velocity a la couzin
def updateV_couzin(a, matrix, maxV):
    dx=0
    dy=0

    #zor
    if matrix[0] != []:
        for agent in matrix[0]:
            disX = agent.point.getX() - a.point.getX()
            disY = agent.point.getY() - a.point.getY()

            rX = disX/absvec(disX, disY)
            rY = disY/absvec(disX, disY)

            dx += rX / absvec(rX, rY)
            dy += rY / absvec(rX, rY)

        dx = -dx
        dy = -dy

    # zoo ; zoa leer
    elif matrix[1] != []  and matrix[2] == []:
        for agent in matrix[1]:
            dx += agent.x_velocity / absvec(agent.x_velocity, agent.y_velocity)
            dy += agent.y_velocity / absvec(agent.x_velocity, agent.y_velocity)
        dx += a.x_velocity / absvec(a.x_velocity, a.y_velocity)
        dy += a.y_velocity / absvec(a.x_velocity, a.y_velocity)

    # zoo leer ; zoa
    elif matrix[1] == []  and matrix[2] != []:
        for agent in matrix[2]:
            disX = agent.point.getX() - a.point.getX()
            disY = agent.point.getY() - a.point.getY()

            rX = disX/absvec(disX, disY)
            rY = disY/absvec(disX, disY)

            dx += rX / absvec(rX, rY)
            dy += rY / absvec(rX, rY)

    # zoo ; zoa
    elif matrix[1] != []  and matrix[2] != []:
        for agent in matrix[1]:
            dx += agent.x_velocity / absvec(agent.x_velocity, agent.y_velocity)
            dy += agent.y_velocity / absvec(agent.x_velocity, agent.y_velocity)
        dx += a.x_velocity / absvec(a.x_velocity, a.y_velocity)
        dy += a.y_velocity / absvec(a.x_velocity, a.y_velocity)

        for agent in matrix[2]:
            disX = agent.point.getX() - a.point.getX()
            disY = agent.point.getY() - a.point.getY()

            rX = disX/absvec(disX, disY)
            rY = disY/absvec(disX, disY)

            dx += rX / absvec(rX, rY)
            dy += rY / absvec(rX, rY)

        dx = 0.5*dx
        dy = 0.5*dy

	# all zones empty
    else:
        dx = a.x_velocity
        dy = a.y_velocity

	# randomness factor / error
    dx += random.uniform(-1, 1)
    dy += random.uniform(-1, 1)

    return [dx, dy]


def update_couzin(agent, agents, maxV, winWidth, winHeight, window, maxTurn, radTurn, negRadTurn,zor_r, zoo_r, zoa_r, blind, speed, free):
        # Velocity update
        for agent  in agents:
            neigh_matrix = neigbour_in_zones(agent, agents, zor_r, zoo_r, zoa_r,blind)
            agent.set_temp_velocity(updateV_couzin(agent,neigh_matrix,maxV))
        # move, draw
        for agent in agents:
            angle_old = math.atan2(agent.y_velocity, agent.x_velocity)
            angle_new = math.atan2(agent.tempvelocity[1], agent.tempvelocity[0])
            alpha = math.degrees(angle_old - angle_new)
            # alpha = calc_angle(agent[1], agent[2],agent[4][0],agent[4][1])
            # test if in ragne of maxturn, if not rotate angle by maxTurn in
            # direction of new direction
            if abs(alpha) > 180:
                if alpha < 0:
                    alpha += 360
                else:
                    alpha -= 360

            if abs(alpha)<maxTurn:
                agent.x_velocity = agent.tempvelocity[0]
                agent.y_velocity = agent.tempvelocity[1]
            elif alpha < 0:
                agent.x_velocity =  agent.x_velocity * math.cos(radTurn) - agent.y_velocity  * math.sin(radTurn)
                agent.y_velocity =  agent.x_velocity * math.sin(radTurn) + agent.y_velocity  * math.cos(radTurn)
            else:
                agent.x_velocity =  agent.x_velocity * math.cos(negRadTurn) - agent.y_velocity  * math.sin(negRadTurn)
                agent.y_velocity =  agent.x_velocity * math.sin(negRadTurn) + agent.y_velocity  * math.cos(negRadTurn)

			# normalise diection vector to 1, and multiply by constant speed
            agent.x_velocity = agent.x_velocity/absvec(agent.x_velocity, agent.y_velocity)  * agent.speed
            agent.y_velocity = agent.y_velocity/absvec(agent.x_velocity, agent.y_velocity)  * agent.speed
            if free:
                agent = checkBoundary_free(agent, winWidth, winHeight)
            else:
                agent = checkBoundary(agent, winWidth, winHeight)

			# draw arrow
            agent.draw_Line()
        return agents
#--------------leadership
def updateV_leadership_couzin(a, matrix, maxV):
    dx=0
    dy=0

    #zor
    if matrix[0] != []:
        for agent in matrix[0]:

            disX = agent[0].point.getX() - a.point.getX()
            disY = agent[0].point.getY() - a.point.getY()

            rX = disX/absvec(disX, disY)
            rY = disY/absvec(disX, disY)

            #flag checkBoundary
            if not agent[1]:
                dx += rX / absvec(rX, rY)
                dy += rY / absvec(rX, rY)
            else:
                dx -= rX / absvec(rX, rY)
                dy -= rY / absvec(rX, rY)
        dx = -dx
        dy = -dy
    	# randomness factor / error
        #dx += random.uniform(-1, 1)
        #dy += random.uniform(-1, 1)
        #normalise
        dx = dx / absvec(dx, dy)
        dy = dy / absvec(dx, dy)


    # zoo ; zoa leer
    elif matrix[1] != []  and matrix[2] == []:
        for b in matrix[1]:
            agent = b[0]
            if not b[1]:
                dx += agent.x_velocity / absvec(agent.x_velocity, agent.y_velocity)
                dy += agent.y_velocity / absvec(agent.x_velocity, agent.y_velocity)
            else:
                dx -= agent.x_velocity / absvec(agent.x_velocity, agent.y_velocity)
                dy -= agent.y_velocity / absvec(agent.x_velocity, agent.y_velocity)

        dx += a.x_velocity / absvec(a.x_velocity, a.y_velocity)
        dy += a.y_velocity / absvec(a.x_velocity, a.y_velocity)
    # zoo leer ; zoa
    elif matrix[1] == []  and matrix[2] != []:
        for b in matrix[2]:
            agent = b[0]
            disX = agent.point.getX() - a.point.getX()
            disY = agent.point.getY() - a.point.getY()
            if not b[1]:
                dx += ( disX / absvec(disX, disY) ) / absvec( disX / absvec(disX, disY) , disY/ absvec(disX,disY) )
                dy += ( disY / absvec(disX, disY) ) / absvec( disX / absvec(disX, disY) , disY/ absvec(disX,disY) )
            else:
                dx -= ( disX / absvec(disX, disY) ) / absvec( disX / absvec(disX, disY) , disY/ absvec(disX,disY) )
                dy -= ( disY / absvec(disX, disY) ) / absvec( disX / absvec(disX, disY) , disY/ absvec(disX,disY) )

    #both zones filled
    elif matrix[1] != []  and matrix[2] != []:

        for b in matrix[1]:
            agent = b[0]

            dx += agent.x_velocity / absvec(agent.x_velocity, agent.y_velocity)
            dy += agent.y_velocity / absvec(agent.x_velocity, agent.y_velocity)
        for b in matrix[2]:
            agent = b[0]
            disX = agent.point.getX() - a.point.getX()
            disY = agent.point.getY() - a.point.getY()

            rX = disX/absvec(disX, disY)
            rY = disY/absvec(disX, disY)

            if not b[1]:
                dx += rX / absvec(rX, rY)
                dy += rY / absvec(rX, rY)
            else:
                dx -= rX / absvec(rX, rY)
                dy -= rY / absvec(rX, rY)

            dx += a.x_velocity / absvec(a.x_velocity, a.y_velocity)
            dy += a.y_velocity / absvec(a.x_velocity, a.y_velocity)

	           # all zones empty
    else:
        dx = a.x_velocity
        dy = a.y_velocity

    # randomness factor / error
    dx += random.uniform(-0.1, 0.1)
    dy += random.uniform(-0.1, 0.1)
    #normalise
    dx = dx / absvec(dx, dy)
    dy = dy / absvec(dx, dy)
    #prefered direction
    x= dx
    y= dy
    dx = (x + (a.weight * a.prefdir[0]) ) / absvec((x + (a.weight * a.prefdir[0])) , (y + (a.weight * a.prefdir[1])))
    dy = (y + (a.weight * a.prefdir[1]) ) / absvec((x + (a.weight * a.prefdir[0])) , (y + (a.weight * a.prefdir[1])))

    return [dx, dy]

def updateV_leadership(a, matrix, maxV):
    dx=0
    dy=0

    #zor
    if matrix[0] != []:
        for agent in matrix[0]:

            disX = agent[0].point.getX() - a.point.getX()
            disY = agent[0].point.getY() - a.point.getY()

            rX = disX/absvec(disX, disY)
            rY = disY/absvec(disX, disY)

            #flag checkBoundary
            if not agent[1]:
                dx += rX / absvec(rX, rY)
                dy += rY / absvec(rX, rY)
            else:
                dx -= rX / absvec(rX, rY)
                dy -= rY / absvec(rX, rY)
        dx = -dx
        dy = -dy
    	# randomness factor / error
        #dx += random.uniform(-1, 1)
        #dy += random.uniform(-1, 1)
        #normalise
        absLen = absvec(dx,dy)
        dx = dx / absLen
        dy = dy / absLen

    else:
    # zoo ; zoa
        zooa = matrix[1] + matrix[2]

        if zooa != []:

            for b in zooa:
                agent = b[0]

                dx += agent[1] / absvec(agent[1], agent[2])
                dy += agent[2] / absvec(agent[1], agent[2])

                disX = agent.point.getX() - a.point.getX()
                disY = agent.point.getY() - a.point.getY()

                rX = disX/absvec(disX, disY)
                rY = disY/absvec(disX, disY)

                if not b[1]:
                    dx += rX / absvec(rX, rY)
                    dy += rY / absvec(rX, rY)
                else:
                    dx -= rX / absvec(rX, rY)
                    dy -= rY / absvec(rX, rY)

            dx += a.x_velocity / absvec(a.x_velocity, a.y_velocity)
            dy += a.y_velocity / absvec(a.y_velocity, a.y_velocity)

	           # all zones empty
        else:
              dx = a.x_velocity
              dy = a.y_velocity

    	# randomness factor / error
        #dx += random.uniform(-1, 1)
        #dy += random.uniform(-1, 1)
        #normalise
        absLen = absvec(dx,dy)
        dx = dx / absLen
        dy = dy / absLen
        #prefered direction
        absLen2 = absvec((dx + (a.weight * a.prefdir[0])) , (dy + (a.weight * a.prefdir[1])))
        dx = (dx + (a.weight * a.prefdir[0]) ) / absLen2
        dy = (dy + (a.weight * a.prefdir[1]) ) / absLen2

    return [dx, dy]

def update_leadership(agent, agents, maxV, winWidth, winHeight, window, maxTurn, radTurn, negRadTurn,zor_r, zoo_r, zoa_r, blind, speed, free, prefDir, prefDir2):
        # Velocity update
        for agent  in agents:
            neigh_matrix = neigbour_in_zones_free(agent, agents, zor_r, zoo_r, zoa_r,blind,winWidth, winHeight)
            agent.set_temp_velocity(updateV_leadership_couzin(agent, neigh_matrix, maxV))
            #agent[4] = updateV_leadership(agent, neigh_matrix, maxV)

        # move, draw
        for agent in agents:
            # alpha = calc_angle(agent[1], agent[2],agent[4][0],agent[4][1])
			# test if in ragne of maxturn, if not rotate angle by maxTurn in
			# direction of new direction

            angle_old = math.atan2(agent.y_velocity, agent.x_velocity)
            angle_new = math.atan2(agent.tempvelocity[1], agent.tempvelocity[0])
            alpha = math.degrees(angle_old - angle_new)

            if abs(alpha) > 180:
                if alpha < 0:
                    alpha += 360
                else:
                    alpha -= 360

            if abs(alpha)<maxTurn:
                agent.set_xvelocity(agent.tempvelocity[0])
                agent.set_yvelocity(agent.tempvelocity[1])
            elif alpha < 0:
                x= agent.x_velocity
                y= agent.y_velocity
                agent.set_xvelocity(x * math.cos(radTurn) - y  * math.sin(radTurn))
                agent.set_yvelocity(x * math.sin(radTurn) + y  * math.cos(radTurn))
            else:                
                x= agent.x_velocity
                y = agent.y_velocity
                agent.set_xvelocity(x * math.cos(negRadTurn) - y  * math.sin(negRadTurn))
                agent.set_yvelocity(x * math.sin(negRadTurn) + y  * math.cos(negRadTurn))


            if (agent.prefdir != [0,0]):
                angle_pref = math.atan2(agent.prefdir[0], agent.prefdir[1])
                angle_new = math.atan2(agent.x_velocity, agent.y_velocity)
                beta = math.degrees(angle_new - angle_pref)

                if abs(beta) > 180:
                    if beta < 0:
                        beta += 360
                    else:
                        beta -= 360

                if abs(beta) < 20 :
                    agent.set_weight(min(1, agent.weight+0.001))
                else:
                    agent.set_weight(max(0, agent.weight-0.000001))

            #agent[1] = agent[4][0]
            #agent[2] = agent[4][1]

			# normalise diection vector to 1, and multiply by constant speed
            x= agent.x_velocity
            y= agent.y_velocity
            agent.x_velocity = x/absvec(x, y)  * agent.speed
            agent.y_velocity = y/absvec(x, y)  * agent.speed
            if free:
                agent = checkBoundary_free(agent, winWidth, winHeight)
            else:
                agent = checkBoundary(agent, winWidth, winHeight)

			# draw arrow
            agent.draw_Line()
            
            #if (agent.prefdir== prefDir):
            #    agent.line.setFill("red")
            #if (agent.prefdir== prefDir2):
            #    agent.line.setFill("blue")
            #agent.line.draw(window)
            
        return agents

#---------------vicsek
def update_vicsek(agent, agents, maxV, winWidth, winHeight, window, zoo_r,speed):
    return update_couzin(agent, agents, maxV, winWidth, winHeight, window, 180, math.radians(180), -math.radians(180), 0, zoo_r, 0, 0,speed, True)


# -----------------plot stuff

def avgdistancetoall(agent,agents):
    i= 0
    totaldistance = 0
    for i in range (len (agents)):
        if agents[i] == agent:
            True
        else:
            totaldistance += distance (agent.point.getX(),agent.point.getY(),agents[i].point.getX(),agents[i].point.getY())
    avgdistance = totaldistance/i
    return avgdistance


def totalavgdistance(agents,numplist):
    for agent in agents:
        numplist = np.append(numplist,avgdistancetoall(agent,agents))
    return numplist

def main():
    winWidth = 500
    winHeight = 500

    window = GraphWin("Window", winWidth, winHeight)

    maxTime = 1000
    maxV = 8
    speed = 4			# constant speed

    blind = 80			# angle of blindness

    maxTurn = 50        # maximum turning angle
    radTurn = math.radians(maxTurn)
    negRadTurn = math.radians(360-maxTurn)
    agents = []
    agentNum = 50

    infNum = 4
    prefDir = [-1 ,-1]
    weight = 0.2

    infNum2 = 2
    prefDir2 = [1 ,1]
    weight2 = 0.2
    with open(TIMESTEPS,'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',quotechar='|',quoting = csv.QUOTE_MINIMAL)
        filewriter.writerow(["Time","X","Y","X Velocity","Y Velocity","weight","desired direction"])


    # data for plotting
    distancedata = np.array([])
    distancestd = np.array([])
    numpycount = np.array([])

    # zones for couzin, vicsek
    # radii of zones
    # swarm: 10, 20, 200
    # torus: 5, 60, 200
    # dynamic parallel group: 5, 100, 200
    # highly parallel group: 5, 180, 200
    zor_r = 5
    zoo_r = 40
    zoa_r = 110
    
    #agents = [0 for y in range(agentNum)]

    #generate point
        # 0 Point
        # 1 XVelocity
        # 2 YVelocity
        # 3 Line
        # 4 temp. VelocityPoint
        # 5 prefered directions
        # 6 weight for pref
    for i in range(agentNum):

        """agent[0] = Point(random.uniform(0,winWidth), random.uniform(0,winHeight))

        agent[1] = random.uniform(-2,2)
        agent[2] = random.uniform(-2,2)

        agent[0].draw(window)
        agent[3] = Line(agent[0], Point(agent[0].getX() + agent[1], agent[0].getY() + agent[2]))
        agent[3].setArrow("last")
        agent[3].draw(window)

        agent[5] = [0,0]
        agent[6] = 0
        """
        agent= Agent(Point(random.uniform(0,winWidth),random.uniform(0,winHeight)),window,speed)
        agents.append(agent)
        agent.set_xvelocity(random.uniform(-2,2))
        agent.set_yvelocity(random.uniform(-2,2))
        agent.draw_Line()
       # print(agent)
    #
    for i in range(infNum):

        agents[i].set_prefdir(prefDir) 
        agents[i].set_weight(weight)
        agents[i].set_colour("red")
    #update points

    for i in range(infNum, infNum + infNum2):
        agents[i].set_prefdir(prefDir2) 
        agents[i].set_weight(weight2)
        agents[i].set_colour("blue")

    for i in range(maxTime):
        for agent in agents:
            with open(TIMESTEPS,'a') as csvfile:
                filewriter = csv.writer(csvfile, delimiter=',',quotechar='|',quoting = csv.QUOTE_MINIMAL)
                filewriter.writerow([i,agent.point.getX(),agent.point.getY(),agent.x_velocity,agent.y_velocity,agent.weight,agent.prefdir])

        #rawdata = totalavgdistance(agents,distancedata)
        #print ("rawdata: "+ str(rawdata))
        #distancedata = np.append(distancedata,np.mean(rawdata))
        #distancestd = np.append(distancestd,np.std(rawdata))
        #numpycount = np.append(numpycount,i)
        #agents = update_simplest(agents, maxV, winWidth, winHeight, window)
        #agents = update_couzin(agent, agents, maxV, winWidth, winHeight, window, maxTurn, radTurn, negRadTurn, zor_r, zoo_r, zoa_r, blind,speed, True)
        #agents = update_vicsek(agent, agents, maxV, winWidth, winHeight, window, zoo_r, speed)
        agents = update_leadership(agent, agents, maxV, winWidth, winHeight, window, maxTurn, radTurn, negRadTurn, zor_r, zoo_r, zoa_r, blind,speed, True, prefDir, prefDir2)
        time.sleep(0.01)

   # plt.errorbar(numpycount,distancedata,yerr=distancestd)
    #plt.show()
    window.getMouse()
    window.close()

main()
