import csv
import math

winWidth = 500
winHeight = 500

zoo_r = 40

def absvec(a, b):
    m = math.sqrt(a*a + b*b)
    if m == 0: m = 0.001
    return m

def distance(xi,xii,yi,yii):
    sq1 = (xi-xii)*(xi-xii)
    sq2 = (yi-yii)*(yi-yii)
    return math.sqrt(sq1 + sq2)

def wraparound_distance(x1, y1, x2, y2):
    xDis = x1-x2
    yDis = y1-y2

    x=x1
    y=y1
    flag = False

    if(xDis > winWidth/2):
        x -= winWidth
        flag = True
    elif (xDis < -winWidth/2):
        x += winWidth
        flag = True
    if(yDis > winHeight/2):
        y -= winHeight
        flag = True
    elif (yDis < -winHeight/2):
        y += winHeight
        flag = True

    return distance(x,x2,y, y2)

def nearest_neighbour(a,aas):
    minDis = float('inf')
    nn = None

    for b in aas:


        if (a == b):
            True
        elif (nn == None):
            nn = b
            dis = wraparound_distance(a[0],a[1],b[0], b[1])
            minDis = dis
        else:
            dis = wraparound_distance(a[0],a[1],b[0], b[1])
            if(dis < minDis):
                minDis = dis
                nn = b
    return nn

def avgdistancetoall(agent,agents):
    i= 0
    totaldistance = 0
    for i in range (len (agents)):
        if agents[i] == agent:
            True
        else:
            totaldistance += wraparound_distance (agent[0],agent[1],agents[i][0],agents[i][1])
    avgd = totaldistance/i
    return avgd


def calc_angle(x1, y1, x2, y2):
        skalar = x1*x2 + y1*y2
        abs1 = absvec(x1, y1)
        abs2 = absvec(x2, y2)

        erg = skalar/(abs1* abs2)
        if erg > 1:
            #print erg
            erg=1

        elif erg < -1:
            #print erg
            erg=-1
        return math.degrees(math.acos(erg))

#-------------------------------------------------
def findClusters(agents1):
    agents = agents1[:]
    clusterSize = 3
    clusters = []
    for agent in agents:
        cluster = [agent]
        agents.remove(agent)
        for agent in cluster:
            found =True
            while(found):
                nn = nearest_neighbour(agent, agents)
                if (nn != None and wraparound_distance(nn[0],nn[1], agent[0], agent[1]) <= zoo_r):

                    cluster.append(nn)
                    agents.remove(nn)
                else:
                    found = False
        if (len(cluster) >= clusterSize ):
            clusters.append(cluster)
    return clusters

CSV = "leader_10_0.2_0_0.2_50.csv"

def avgDistance_parse(CSV):
    with open(CSV, 'r') as csvfile:
         reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
         dreader = csv.DictReader(csvfile)
         i = 0
         agents = []
         disSum = 0
         for row in dreader:
             if row['Time'] == str(i):
                 agents.append([float(row['x']), float(row['y']) ])
             else:
                 for agent in agents:
                     disSum += avgdistancetoall(agent, agents)
                 print (str(i)+": "+str(disSum/len(agents)))
                 disSum = 0
                 agents = []
                 agents.append([float(row['x']), float(row['y']) ])

                 i+=1


def avgWeight_parse(CSV):
    with open(CSV, 'r') as csvfile:
         reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
         dreader = csv.DictReader(csvfile)
         i = 0
         weightSum = 0
         j = 0

         for row in dreader:
            if row['Time'] == str(i):
                if abs(int(row['desiredX'])) + abs(int(row['desiredY'])) != 0:
                    j += 1
                    weightSum += float( row['weigth'])

            else:

                print str(i) + ": " + str(weightSum/(j*1.0))
                i += 1
                weightSum = 0
                j = 0
                if abs(int(row['desiredX'])) + abs(int(row['desiredY'])) != 0:
                    j += 1
                    weightSum += float( row['weigth'])


def avgDeltaPref_parse(CSV):
    with open(CSV, 'r') as csvfile:
         reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
         dreader = csv.DictReader(csvfile)
         i = 0
         deltaPrefSum = 0
         j = 0

         for row in dreader:
            if row['Time'] == str(i):
                if abs(int(row['desiredX'])) + abs(int(row['desiredY'])) != 0:
                    j += 1
                    deltaPrefSum += calc_angle(float(row['x_velocity']), float(row['y_velocity']),float(row['desiredX']), float(row['desiredY']) )

            else:

                print str(i) + "avg delta prefDir: " + str(deltaPrefSum/(j*1.0))
                i += 1
                deltaPrefSum = 0
                j = 0
                if abs(int(row['desiredX'])) + abs(int(row['desiredY'])) != 0:
                    j += 1
                    deltaPrefSum += calc_angle(float(row['x_velocity']), float(row['y_velocity']),float(row['desiredX']), float(row['desiredY']) )


def clusters_parse(CSV):
    with open(CSV, 'r') as csvfile:
         reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
         dreader = csv.DictReader(csvfile)
         i = 0
         agents = []
         clusters = []

         for row in dreader:
             if row['Time'] == str(i):
                 agents.append([float(row['x']), float(row['y']) ])

             else:
                 #print "agents1: " + str(len(agents))

                 clusters = findClusters(agents)
                 #print "agents: " + str(len(agents))

                 print i
                 print len(clusters)
                 print avgDis_cluster(clusters)

                 clusters = []
                 agents = []
                 i+=1
                 agents.append([float(row['x']), float(row['y']) ])

def avgDir_parse(CSV):
        with open(CSV, 'r') as csvfile:
             reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
             dreader = csv.DictReader(csvfile)
             i = 0
             agents = []
             dirX = 0
             dirY = 0
             for row in dreader:
                 if row['Time'] == str(i):
                     dirX += float(row['x_velocity'])
                     dirY += float(row['y_velocity'])
                     agents.append([float(row['x_velocity']), float(row['y_velocity']) ])
                 else:

                     #print (str(i)+": X "+str(dirX/len(agents)))
                     #print (str(i)+": Y "+str(dirY/len(agents)))
                     avgAngle = (math.atan2(dirY/len(agents),dirX/len(agents)))
                     angleSum = 0
                     for agent in agents:
                         #print math.atan2(agent[1], agent[0])
                         angleSum += (avgAngle - (math.atan2(agent[1], agent[0])))**2
                     confusion = angleSum/ len(agents)*1.0
                     confusion = math.sqrt(confusion)

                     print i
                     print "confusion " + str(confusion)

                     dirX = 0
                     dirY = 0
                     agents = []
                     dirX += float(row['x_velocity'])
                     dirY += float(row['y_velocity'])
                     agents.append([float(row['x_velocity']), float(row['y_velocity']) ])
                     i+=1

def avgDis_cluster(clusters):
    avgDis = []
    for cluster in clusters:
        disSum = 0
        for agent in cluster:
            disSum += avgdistancetoall(agent, cluster)
        avgDis.append(disSum/ len(cluster)*1.0)
    return avgDis

avgDir_parse(CSV)
#l = [[1,1],[2,2], [3,3], [200,200], [200, 201], [210, 201] ]
#clusters = findClusters(l)
#print nearest_neighbour(l[0],l)
